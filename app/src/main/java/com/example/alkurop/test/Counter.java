package com.example.alkurop.test;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;


public class Counter {
  private final Observable<String> timer;

  Counter(int intervalMillis) {
    timer = Observable
        .interval(intervalMillis, TimeUnit.MILLISECONDS)
        .map(new Function<Long, String>() {
          @Override
          public String apply(Long aLong) throws Exception {
            return String.valueOf(aLong);
          }
        })
        .share();
  }


  public Observable<String> getAsObservable() {
    return timer;
  }

  public Observable<String> getAsObservableWithDelay(final int millis) {
    return getAsObservable()
        .concatMap(new Function<String, ObservableSource<? extends String>>() {
          @Override
          public ObservableSource<? extends String> apply(String s) throws Exception {
            return Observable
                .just(s)
                .delay(millis, TimeUnit.MILLISECONDS);
          }
        });

  }
}
