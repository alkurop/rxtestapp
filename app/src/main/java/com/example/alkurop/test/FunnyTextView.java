package com.example.alkurop.test;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by alkurop on 4/13/17.
 */

public class FunnyTextView extends AppCompatTextView {

  private static final int RED = Color.parseColor("#ff0000");
  private static final int BLUE = Color.parseColor("#2557f4");
  private static final int YELLOW = Color.parseColor("#e4eb29");

  private final int[] COLORS_1 = {RED, YELLOW, BLUE};
  private final int[] COLORS_2 = {BLUE, RED, YELLOW};
  private final int[] COLORS_3 = {YELLOW, BLUE, RED};

  private static final int DURATION_MILLIS = 500;
  private static final int PERIOD = 2;

  private int state = 0;

  CompositeDisposable compositeDisposable = new CompositeDisposable();

  public FunnyTextView(Context context) {
    super(context);
  }

  public FunnyTextView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public FunnyTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    init();
  }

  @Override
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    compositeDisposable.clear();
  }

  private void init() {
    Disposable disposable = Observable.interval(PERIOD, TimeUnit.SECONDS)
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<Long>() {
          @Override
          public void accept(Long aLong) throws Exception {
            TransitionDrawable transitionDrawable = new TransitionDrawable(getLayers());
            setBackgroundDrawable(transitionDrawable);
            transitionDrawable.startTransition(DURATION_MILLIS);
          }
        }, new Consumer<Throwable>() {
          @Override
          public void accept(Throwable throwable) throws Exception {
            throwable.printStackTrace();
          }
        });
    compositeDisposable.addAll(disposable);
    setBackgroundDrawable(getState1()[state]);
  }

  public Drawable[] getLayers() {
    Drawable[] layers;
    if (state == 0) {
      state = 1;
      layers = getState1();
    } else if (state == 1) {
      state = 2;
      layers = getState2();
    } else {
      state = 0;
      layers = getState3();
    }
    return layers;
  }

  private Drawable[] getState1() {
    return new Drawable[]{
        new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, COLORS_1),
        new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, COLORS_2)
    };
  }

  private Drawable[] getState2() {
    return new Drawable[]{
        new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, COLORS_2),
        new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, COLORS_3)
    };
  }

  private Drawable[] getState3() {
    return new Drawable[]{
        new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, COLORS_3),
        new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, COLORS_1)
    };
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    int width = getMeasuredWidth();
    int height = getMeasuredHeight();

    int commonSide = width > height ? width : height;
    setMeasuredDimension(commonSide, commonSide);
  }
}
