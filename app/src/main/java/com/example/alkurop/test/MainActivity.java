package com.example.alkurop.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
  public static final int DELAY = 200;
  private static final Integer INTERVAL_MILLIS = 100;

  CompositeDisposable compositeDisposable;

  TextView tv1;
  TextView tv2;
  TextView tv3;

  Consumer<String> listener1;
  Consumer<String> listener2;
  Consumer<String> listener3;

  Consumer<Throwable> listenerError;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    tv1 = ((TextView) findViewById(R.id.tv1));
    tv2 = ((TextView) findViewById(R.id.tv2));
    tv3 = ((TextView) findViewById(R.id.tv3));

    compositeDisposable = new CompositeDisposable();

    initListeners();
    observeTimer();
  }

  private void initListeners() {
    listener1 = new Consumer<String>() {
      @Override
      public void accept(String s) throws Exception {
        tv1.setText(s);
      }
    };
    listener2 = new Consumer<String>() {
      @Override
      public void accept(String s) throws Exception {
        tv2.setText(s);
      }
    };
    listener3 = new Consumer<String>() {
      @Override
      public void accept(String s) throws Exception {
        tv3.setText(s);
      }
    };

    listenerError = new Consumer<Throwable>() {
      @Override
      public void accept(Throwable throwable) throws Exception {
        throwable.printStackTrace();
      }
    };
  }


  private void observeTimer() {
    Disposable disposable1 = getCounterObservable()
        .subscribe(listener1, listenerError);

    Disposable disposable2 = getCounterObservable()
            .subscribe(listener2, listenerError);

    Disposable disposable3 = getCounterObservable()
        .subscribe(listener3, listenerError);

    compositeDisposable.addAll(disposable1, disposable2, disposable3);
  }

  private Observable<String> getCounterObservable() {
    return new Counter(INTERVAL_MILLIS)
        .getAsObservableWithDelay(DELAY)
        .observeOn(AndroidSchedulers.mainThread());
  }

  @Override
  protected void onDestroy() {
    compositeDisposable.clear();
    super.onDestroy();
  }
}
