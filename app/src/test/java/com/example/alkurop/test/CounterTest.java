package com.example.alkurop.test;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.observers.TestObserver;


public class CounterTest {
  private static final int INTERVAL_MILLIS = 100;
  private TestObserver<String> observer = new TestObserver<>();
  private TestObserver<String> observer2 = new TestObserver<>();
  private Counter counter;

  @Before
  public void initTest() {
    counter = new Counter(INTERVAL_MILLIS);
  }

  @Test
  public void testIncomingIsSequential() throws Exception {

    //When
    counter.getAsObservable().subscribe(observer);
    counter.getAsObservable().subscribe(observer2);

    //Then
    Thread.sleep((long) (INTERVAL_MILLIS * 10.5));

    String[] expectedSequence = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    observer.assertValues(expectedSequence);
    observer2.assertValues(expectedSequence);
  }

  @Test
  public void testIncomingIsDelayed() throws Exception {

    //When
    counter.getAsObservable().subscribe(observer);
    counter.getAsObservableWithDelay(INTERVAL_MILLIS * 2).subscribe(observer2);


    Thread.sleep((long) (INTERVAL_MILLIS * 10));
    String[] expectedSequence = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    String[] expectedSequenceDelay= {"0", "1", "2", "3"};

    observer.assertValues(expectedSequence);
    observer2.assertValues(expectedSequenceDelay);
  }
}
